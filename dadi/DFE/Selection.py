"""
Distribution functions to wrap demographics with scalar selection. Most
of this code is modified dadi code, and the selection stuff is a 
modified version of the script found at: 
https://groups.google.com/forum/#!topic/dadi-user/4xspqlITcvc

There are a few changes to the integration, so that anything below the
lower bound is assumed to be effectively neutral; and anything above
the lower bound is assumed to not be segregating, and weighted 0.

I added in multiprocessing capabilities because generating all the 
spectra takes really long for large values of gamma. One workaround
is to not integrate where the gammas are large, since the SFSes there
should be close to 0... 
"""

import os
import sys
import operator
import numpy
from numpy import logical_and, logical_not
from scipy.special import gammaln
import scipy.stats.distributions
import scipy.integrate
import scipy.optimize
from dadi import Numerics, Inference, Misc
from dadi.Spectrum_mod import Spectrum


class spectra:
    def __init__(self, params, ns, demo_sel_func, pts_l=None, 
                 Npts=500, int_breaks=None, 
                 additional_gammas=[],
                 int_bounds=(1e-4, 1000.), mp=False, echo=False, cpus=None):
        """
        params: optimized demographic parameters, don't include gamma 
        here
        demo_sel_func: dadi demographic function with selection. gamma
        must be the last argument.
        ns: sample sizes
        Npts: number of grid points over which to integrate
        steps: use this to set break points for spacing out the 
        intervals
        mp: True if you want to use multiple cores (utilizes 
        multiprocessing) if using the mp option you must also specify
        # of cpus, otherwise this will just use nthreads-1 on your
        machine
        """
        self.ns = ns
        self.spectra = []

        #create a vector of gammas that are log-spaced over sequential 
        #intervals or log-spaced over a single interval.
        if not (int_breaks is None):
            numbreaks = len(int_breaks)
            stepint = Npts/(numbreaks-1)
            self.gammas = []
            for i in reversed(range(0,numbreaks-1)):
                self.gammas = numpy.append(
                    self.gammas, -numpy.logspace(numpy.log10(int_breaks[i+1]),
                                                 numpy.log10(int_breaks[i]),
                                                 stepint))
        else:
            self.gammas = -numpy.logspace(numpy.log10(int_bounds[1]),
                                          numpy.log10(int_bounds[0]), Npts)

        # Record negative gammas, for later use
        self.neg_gammas = self.gammas
        # Add additional gammas to array
        self.gammas = numpy.concatenate((self.gammas, additional_gammas))
        self.pts_l = pts_l
        func_ex = Numerics.make_extrap_func(demo_sel_func)
        self.params = tuple(params)

        if not mp: #for running with a single thread
            for ii,gamma in enumerate(self.gammas):
                self.spectra.append(func_ex(tuple(params)+(gamma,), self.ns,
                                            self.pts_l))
                if echo:
                    print '{0}: {1}'.format(ii, gamma)
        else: #for running with with multiple cores
            import multiprocessing
            if cpus is None:
                cpus = multiprocessing.cpu_count() - 1
            def worker_sfs(in_queue, outlist, popn_func_ex, params, ns,
                           pts_l):
                """
                Worker function -- used to generate SFSes for
                single values of gamma.
                """
                while True:
                    item = in_queue.get()
                    if item == None:
                        return
                    ii, gamma = item
                    sfs = popn_func_ex(tuple(params)+(gamma,), ns, pts_l)
                    print '{0}: {1}'.format(ii, gamma)
                    result = (gamma, sfs)
                    outlist.append(result)
            manager = multiprocessing.Manager()
            results = manager.list()
            work = manager.Queue(cpus)
            pool = []
            for i in xrange(cpus):
                p = multiprocessing.Process(target=worker_sfs,
                                            args=(work, results, func_ex,
                                            params, self.ns, self.pts_l))
                p.start()
                pool.append(p)
            for ii,gamma in enumerate(self.gammas):
                work.put((ii, gamma))
            for jj in xrange(cpus):
                work.put(None)
            for p in pool:
                p.join()
            reslist = []
            for line in results:
                reslist.append(line)
            reslist.sort(key = operator.itemgetter(0))
            for gamma, sfs in reslist:
                self.spectra.append(sfs)

        #self.neu_spec = demo_sel_func(params+(0,), self.ns, self.pts)
        self.neu_spec = func_ex(tuple(params)+(0,), self.ns, self.pts_l)
        self.extrap_x = self.spectra[0].extrap_x
        self.spectra = numpy.array(self.spectra)

    def integrate_old(self, params, sel_dist, theta):
        """
        integration without re-normalizing the DFE. This assumes the
        portion of the DFE that is not integrated is not seen in your
        sample.
        """
        #need to include tuple() here to make this function play nice
        #with numpy arrays
        sel_args = (self.gammas,) + tuple(params)
        #compute weights for each fs
        weights = sel_dist(*sel_args)

        #compute weight for the effectively neutral portion. not using
        #CDF function because I want this to be able to compute weight
        #for arbitrary mass functions
        weight_neu, err_neu = scipy.integrate.quad(sel_dist, self.gammas[-1],
                                                   0, args=tuple(params))

        #function's adaptable for demographic models from 1-3 populations
        pops = len(self.neu_spec.shape)
        if pops == 1:
            integrated = self.neu_spec*weight_neu + Numerics.trapz(
                weights[:,numpy.newaxis]*self.spectra, self.gammas, axis=0)
        elif pops == 2:
            integrated = self.neu_spec*weight_neu + Numerics.trapz(
                weights[:,numpy.newaxis,numpy.newaxis]*self.spectra,
                self.gammas, axis=0)
        elif pops == 3:
            integrated = self.neu_spec*weight_neu + Numerics.trapz(
                weights[:,numpy.newaxis,numpy.newaxis,numpy.newaxis]*self.spectra,
                self.gammas, axis=0)
        else:
            raise IndexError("Must have one to three populations")

        integrated_fs = Spectrum(integrated, extrap_x=self.extrap_x)

        #no normalization, allow lethal mutations to fall out
        return integrated_fs * theta

    def integrate(self, params, sel_dist, theta, exterior_int=True):
        """
        integration without re-normalizing the DFE. This assumes the
        portion of the DFE that is not integrated is not seen in your
        sample.
        """
        #need to include tuple() here to make this function play nice
        #with numpy arrays
        sel_args = (self.neg_gammas,) + tuple(params)
        #compute weights for each fs
        weights = sel_dist(*sel_args)

        # Restrict ourselves to negative gammas
        Nneg = len(self.neg_gammas)
        spectra = self.spectra[:Nneg]

        #function's adaptable for demographic models from 1-3 populations
        pops = len(self.neu_spec.shape)
        if pops == 1:
            integrated = Numerics.trapz(weights[:,numpy.newaxis]*spectra, 
                                        self.neg_gammas, axis=0)
        elif pops == 2:
            integrated = Numerics.trapz(weights[:,numpy.newaxis,numpy.newaxis]*spectra,
                self.neg_gammas, axis=0)
        elif pops == 3:
            integrated = Numerics.trapz(
                weights[:,numpy.newaxis,numpy.newaxis,numpy.newaxis]*spectra,
                self.neg_gammas, axis=0)
        else:
            raise IndexError("Must have one to three populations")

        integrated_fs = Spectrum(integrated, extrap_x=self.extrap_x)

        if not exterior_int:
            return integrated_fs * theta

        smallest_gamma = self.neg_gammas[-1]
        largest_gamma = self.neg_gammas[0]
        #compute weight for the effectively neutral portion. not using
        #CDF function because I want this to be able to compute weight
        #for arbitrary mass functions
        weight_neu, err_neu = scipy.integrate.quad(sel_dist, smallest_gamma,
                                                   0, args=tuple(params))
        # compute weight for the effectively lethal portion
        weight_del, err = scipy.integrate.quad(sel_dist, -numpy.inf, largest_gamma,
                                               args=tuple(params))

        integrated_fs += self.neu_spec*weight_neu
        integrated_fs += spectra[0]*weight_del

        #no normalization, allow lethal mutations to fall out
        return integrated_fs * theta

    def integrate_point_pos(self, params, sel_dist, theta, func_ex):
        """
        Integrate including a term for point mass positive selection.

        The last two terms are assumed to be the proportion of positive
        selection and the gamma for that point mass, respectively. The remaining
        parameters are for the continuous sel_dist.
        """
        pdf_params, ppos, gammapos = params[:-2], params[-2], params[-1]

        pdf_fs = self.integrate(pdf_params, sel_dist, theta)

        if gammapos not in self.gammas:
            pos_fs = theta*func_ex(tuple(self.params) + (gammapos,),
                                   self.ns, self.pts_l)
            self.gammas = numpy.append(self.gammas, gammapos)
            self.spectra = numpy.append(self.spectra, [pos_fs.data], axis=0)
        ii = list(self.gammas).index(gammapos)
        pos_fs = Spectrum(self.spectra[ii], extrap_x=self.extrap_x)

        return (1-ppos)*pdf_fs + ppos*pos_fs

    def integrate_norm(self, params, sel_dist, theta):
        """
        """
        #need to include tuple() here to make this function play nice
        #with numpy arrays
        #compute weights for each fs
        sel_args = (self.gammas,) + tuple(params)
        weights = sel_dist(*sel_args)

        #compute weight for the effectively neutral portion. not using
        #CDF function because I want this to be able to compute weight
        #for arbitrary mass functions
        weight_neu, err_neu = scipy.integrate.quad(sel_dist, self.gammas[-1],
                                                   0, args=tuple(params))

        #function's adaptable for demographic models from 1-3
        #populations but this assumes the selection coefficient is the
        #same in both populations
        pops = len(self.neu_spec.shape)
        if pops == 1:
            integrated = self.neu_spec*weight_neu + Numerics.trapz(
                weights[:,numpy.newaxis]*self.spectra, self.gammas, axis=0)
        elif pops == 2:
            integrated = self.neu_spec*weight_neu + Numerics.trapz(
                weights[:,numpy.newaxis,numpy.newaxis]*self.spectra,
                self.gammas, axis=0)
        elif pops == 3:
            integrated = self.neu_spec*weight_neu + Numerics.trapz(
                weights[:,numpy.newaxis,numpy.newaxis,numpy.newaxis]*self.spectra,
                self.gammas, axis=0)
        else:
            raise IndexError("Must have one to three populations")

        integrated_fs = Spectrum(integrated, extrap_x=self.extrap_x)

        #normalization
        dist_int = Numerics.trapz(weights, self.gammas) + weight_neu
        return integrated_fs/dist_int * theta


#define a bunch of default distributions just to make everything easier
def gamma_dist(mgamma, alpha, beta):
    """
    x, shape, scale
    """
    return scipy.stats.distributions.gamma.pdf(-mgamma, alpha, scale=beta)


def beta_dist(mgamma, alpha, beta):
    """
    x, alpha, beta
    """
    return scipy.stats.distributions.beta.pdf(-mgamma, alpha, beta)


def exponential_dist(mgamma, scale):
    return scipy.stats.distributions.expon.pdf(-mgamma, scale=scale)


def lognormal_dist(mgamma, mu, sigma, scal_fac=1):
    return scipy.stats.distributions.lognorm.pdf(
        -mgamma, sigma, scale=numpy.exp(mu + numpy.log(scal_fac)))


def normal_dist(mgamma, mu, sigma):
    return scipy.stats.distributions.norm.pdf(-mgamma, loc=mu, scale=sigma)

def neugamma(mgamma, p, alpha, beta):
        mgamma=-mgamma
        if (0 <= mgamma) and (mgamma < -smallgamma):
                return p/(-smallgamma) + (1-p)*dadi.Selection.gamma_dist(
                    -mgamma,alpha, beta)
        else:
                return dadi.Selection.gamma_dist(-mgamma, alpha, beta) * (1-p)
